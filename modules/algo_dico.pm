#============================================================
# Package :	algo_dico.pm
# Auteur : 	Dorian Venderick <dorian.venderick@student.uclouvain.be>
# Date : 	09/06/2019
#============================================================
# Ce package tente de deviner la langue d'un texte donné en entrée en comparant les mots présents dans les dictionnaires de langues enregistrés.
# La langue choisie est celle qui a le plus de mots en commun dans le dictionnaire et dans le texte d'entrée
#============================================================
# Usage :
# doit être importée dans le fichier principal ../langguess.pl
# - Pour enregistrer un dictionnaire :  perl langguess.pl --train --dico LANGUE mon-dico.dic
#
#============================================================
package algo_dico;
use strict;
use warnings;
use utf8;
use Sereal;
binmode(STDOUT,':encoding(UTF-8)');

my $dicos_path = "$FindBin::Bin/data/dicos/bin_dicos/"; # chemin ou les dictionnaires est enregistré
my $dicos_sereal_name = "trained_dicos"; # Le nom sous lequel le fichier binaire Sereal sera/est sauvé

sub guessLang {
  # -----------------------------------------
  # Devine le langage du texte d'entrée en comptant la présence des mots
  # dans les dictionnaires enregistrés
  # -----------------------------------------
  # pre : $input_file en argument, le fichier pour lequel on cherche la langue
  # post : retourne la langue la plus probable
  my $input_file = shift; #PRECONDITION 1
  my $trained_dicos = load_stored_hash(); # On charge les dictionnaires à partir
  my $comparatif_langues = {};

  # on ouvre le fichier d'input
  open( my $input_text, '<', $input_file )
   or die( "Impossible d'ouvrir $input_file" );
  binmode( $input_text, ':encoding(UTF-8)' );

  while ( my $ligne = <$input_text> ) {
    chomp( $ligne );
    my @words = split(/ /, $ligne);
    foreach my $word ( @words ){
      foreach my $langue ( keys( %{$trained_dicos} ) ){
        if ( $trained_dicos->{$langue}{$word} ){
          if ( !$comparatif_langues->{$langue} ){
            $comparatif_langues->{$langue} = 1;
          } else {
            $comparatif_langues->{$langue}++;
          }
        }
      }
    }
  }
  close($input_text);
  return bestlanguagematch($comparatif_langues);
}
sub bestlanguagematch {
  # pre : $langues  REF(Hash) : réfère à un hash contenant les probabilités des langues
  # post : imprime et retourne la langue avec le score le plus élevé
  my $langues = shift;
  my $max_value = 0;
  my $bestmatch = undef;

  while (my ($langue,$valeur)= each %{$langues}){
    if ($valeur >= $max_value) {
      $max_value = $valeur;
      $bestmatch = $langue;
    }
  }
  if ($bestmatch){
    print ("\t" . $bestmatch . "\n") ;
    return $bestmatch;
  } else {
    print("Aucune langue n'a pu être identifiée\n");
    return -1;
  }
}
sub add_dico {
  # -----------------------------------------
  # Prends un dictionnaire en entrée, le traîte, et enregistre un hash correspondant
  # -----------------------------------------
  # pre :
  #   - $training_lgg (STR) : Langue à 'entraîner'
  #   - $input_file  (STR) : nom de fichier dictionnaire. 1 mot par lign suivi de ','
  #       ex de ligne : Abend,.N:aeM:deM:neM
  #       (dans l'absolu, la partie après la virgule n'est pas nécessaire)
  # post :
  #   - Sauvegarde dans le fichier binaire des dictionnaires Sereal
  my $training_lgg = shift;
  my $input_file = shift;
  my $trained_dicos = load_stored_hash();
  my $dico_hash = generate_hash( $input_file );

  #printhash ($dico_hash);
  $trained_dicos->{$training_lgg} = $dico_hash;
  return save_dicos_hash($trained_dicos);
}
sub save_dicos_hash {
  # -----------------------------------------
  # Enregistre la strucutre hash des dictionnaire avec Sereal
  # -----------------------------------------
  # pre :
  #     - $trained_dicos REF(%HASH) : une référence à un hash, qui contient les dicos.
  # post :
  #     - enregistre les dico sous 1 fichier binaire avec Sereal, nom :
  #       $dicos_sereal_name (variable "globale" du module)
  #     - return la ref. du hash
  my $trained_dicos = shift;
  Sereal::write_sereal_file($dicos_path . $dicos_sereal_name, $trained_dicos);
  return $trained_dicos;
}
sub load_stored_hash {
  # vérifie s'il y a un fichier de dicos déjà enregistrer, le charger en mémoire
  # et renvoit sa référence, ou bien renvoit la référence d'un hash vide.
  # -----------------------------------
  # pre : /
  # post : renvoit la référence d'un hash

  my $saved_dicos = {}; # un nouveau tableau de hachage, dans la cas où il n'y a pas de fichier Sereal enregistré

  opendir(my $dir, $dicos_path) or die("...");
  while (my $file = readdir($dir)) {
    if ( $file eq $dicos_sereal_name ){
      $saved_dicos = Sereal::read_sereal($dicos_path . $dicos_sereal_name);
    }
  }
  closedir($dir);

  return $saved_dicos;
}
sub generate_hash {
  # -----------------------------------------
  # Prends un dictionnaire en entrée, renvoit un REF(hash) avec les mots de ce dictionnaire
  # -----------------------------------------
  # pre :
  #   - $input_file  (STR) : nom de fichier dictionnaire. 1 mot par lign suivi de ','
  #       ex de ligne : Abend,.N:aeM:deM:neM
  #       (dans l'absolu, la partie après la virgule n'est pas nécessaire)
  # post :
  #   - return REF(hash) contenant uniquement les mots du dictionnaire
  my $input_file = shift;

  open( my $input_text, '<', $input_file )
   or die( "Impossible d'ouvrir $input_file" );
  binmode( $input_text, ':encoding(UTF-8)' );

  my $dico_hash = {};

  while ( my $ligne = <$input_text> ) {
    chomp( $ligne );
    my @temp = split(/,/, $ligne);
    # certains dictionnaires comportant des entrées avec espaces, nous on sépare en entrée différentes
    @temp = split(/ /, $temp[0]);
    foreach my $elem ( @temp ){
      $dico_hash->{lc($elem)} = 1;
    }
  }
  close($input_text);
  return $dico_hash;
}

sub printhash {
  # ----------------------
  # print le contenu d'un hache (si plusieur hash imbriqué par référence,
  # n'entre pas dans les sous niveaux).
  # ----------------------
  my $hash = shift;
  while (my ($k,$v)=each %{$hash}){
    print "\t$k \t$v\n";
  }
}
sub list {
  # pre : /
  # post : renvoit les langues possibles pour cet algorithme en REF(@TABLEAU)

  return [ sort ( keys ( %{load_stored_hash()} ) ) ] ;
}
sub print_list {
  # imprime les langues disponibles dans cet algorithme
  # pre : /
  # post print les langues disponibles
  my $list = list();
  print("== L'algorithme 'DICO' peut détecter les langues suivantes : \n");
  my $i = 1;
  foreach my $langue ( @{$list} ){
    print("\t$i.  $langue\n");
    $i++;
  }
}
1;
