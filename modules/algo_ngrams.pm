#============================================================
# Package :	algo_ngrams.pm

# Auteur : 	Dorian Venderick <dorian.venderick@student.uclouvain.be>
# Date : 	09/06/2019
#============================================================
# Ce package tente de deviner la langue d'un texte donné en entrée avec une approche statistique de ngrams de caractères.
# Les ngrams de caractères sont comparé à la base de donnée de langue.
# La langue ayant les probabilité d'ngrams la plus proche de celle du texte d'entrée est choisie.
# 1) Permet de générer et sauvegardre la matrice des ngrams d'un langue donnée
# 2) Permet de générer la matric des ngrams d'un text et compare avec celles dans langues enregistrées pour en resortir la plus probable
#============================================================
# Usage :
# doit être importée dans le fichier principal ../langguess.pl
# perl langguess.pl --train --ngram LANGUE input.txt
#============================================================
package algo_ngrams;
use strict;
use utf8;
use Sereal;
binmode(STDOUT,':encoding(UTF-8)');

my $ngrams_sereal_name = "trained_ngrams"; # Le nom sous lequel le fichier binaire Sereal sera/est sauvé

sub guessLang {
  # -----------------------------------------
  # Devine le langage du texte d'entrée en fonction d'un matrice de ngrams
  # -----------------------------------------
  # pre : $input_file en argument, le fichier pour lequel l'on cherche la langue
  # post : retourne la langue la plus probable
  my $input_file = shift; #PRECONDITION

  my $trained_ngrams_hash = load_stored_hash(); #REF hash
  my $comparatif_langues = compare_lggs_matrix($input_file, $trained_ngrams_hash); #contiendra le score par langue

  return bestlanguagematch($comparatif_langues);


  sub compare_lggs_matrix {
    # -----------------------------------------
    # Génère un REF(%hash) de la probabilité par langue, selon les n-grams
    # -----------------------------------------
    # pre :
    #     - $input_file (STR): texte d'entrée
    #      - $trained_ngrams_hash (REF(%HASH)) : ngrams entraînés
    # post :
    #     - return un REF(%hash) des langues les plus probables
    # -----------------------------------------

    my $input_file = shift; #PRECONDITION 1
    my $trained_ngrams_hash  = shift; #PRECONDITION 2

    if ( !%{$trained_ngrams_hash} ){
      # Si le hash d'entraînement est vide
      print ("L'algorithme NGRAM n'a pas encore été etraîné, aucune langue n'est identifié\n");
      print ("utilisez l'option -h pour avoir plus de renseignement sur l'entraînement de n-grams de caractères\n");
    } else {
      # Si au moins une langue est entraînée
      my $ngrams_input = generateMatrix($input_file); #REF hash

      my $lgg_proba = {}; # contiendra les langues plausible selon les comparaisons de probabilité d'Ngrams
      foreach my $in_ngram ( keys( %{$ngrams_input} ) ){
        my $closest_lgg = undef; # indiquera la langue la plus plausible pour le n-gram courant
        my $min_rapport = undef ; # La différence minimum entre le n_gram du fichier

        foreach my $trained_lgg ( keys(%{$trained_ngrams_hash}) ) {
          if ($trained_ngrams_hash->{$trained_lgg}{$in_ngram}) {
            # Si on trouve le n-gram dans la matrice d'entraînement
            my $c_rapport = abs($ngrams_input->{$in_ngram} - $trained_ngrams_hash->{$trained_lgg}{$in_ngram}) ;
            if ( $min_rapport ) {
              # si min_rapport a été initialisé
              $min_rapport = $c_rapport;
              $closest_lgg = $trained_lgg;
            } else {
              # si min_rapport n'a pas encore été initialisé
              $min_rapport = $c_rapport;
              $closest_lgg = $trained_lgg;
            }
          }
        }
        if ($closest_lgg){
          # si une langue proche a été trouvé
          if ( $lgg_proba->{$closest_lgg} ) {
            $lgg_proba->{$closest_lgg}++;
          } else {
            $lgg_proba->{$closest_lgg} = 1;
          }
        }
      }
      return $lgg_proba;
    }
  }
}
sub bestlanguagematch {
  # pre : $langues  REF(Hash) : réfère à un hash contenant les probabilités des langues
  # post : imprime et retourne la langue avec le score le plus élevé
  my $langues = shift;
  my $max_value = 0;
  my $bestmatch = undef;

  while (my ($langue,$valeur)= each %{$langues}){
    if ($valeur >= $max_value) {
      $max_value = $valeur;
      $bestmatch = $langue;
    }
  }
  if ($bestmatch){
    print ("\t" . $bestmatch . "\n") ;
    return $bestmatch;
  } else {
    print("Aucune langue n'a pu être identifiée\n");
    return -1;
  }
}
sub train {
  # créer une matrice des ratios des ngrams présents dans le texte donné en entrée.
  # Crée un fichier binaire Sereal s'il n'existe pas déjà et y enregistre la tale de hachage
  # exemple de structure :
  #   {
  #     "francais"  =>
  #        {
  #           "ab" = 0.2,
  #           ...
  #        },
  #     "anglais"   => {...}
  #   }
  # -----------------------------------------
  # pre :
  #   - $targetlgg    (STR) : La langue a entraîner (ne peut contenir que de lettre, chiffres et underscore (/\w/))
  #   - $input_file   (STR) : en argument, le fichier d'entrainement
  # post :
  #   - sauvegarde le la table de hachage sur le disque dur
  my $targetlgg   = lc(shift);
  if ( $targetlgg !~ /^\w+$/ ){
    print( "le nom de la langue ne peut pas contenir de caractères spéciaux (sauf underscore).\n");
    print( "Ex : 'français' doit être écrit 'francais' \n" );
    return;
  }
  my $input_file  = shift;

  my $n = 2; #2 pour bi-grams

  my $ngramhash = generateMatrix($input_file); # générer la matrice de probabilité des n-grams

  my $trained_ngrams_hash = load_stored_hash();
  $trained_ngrams_hash->{$targetlgg} = $ngramhash;
  Sereal::write_sereal_file("$FindBin::Bin/data/ngrams/$ngrams_sereal_name", $trained_ngrams_hash);
}

sub load_stored_hash {
  # vérifie s'il y a un fichier de ngrams déjà enregistrer, le charge en mémoire
  # et renvoit sa référence, ou bien renvoit la référence d'un hash vide.
  # -----------------------------------
  # pre : /
  # post : renvoit la référence d'un hash

  my $trained_ngrams_hash = {}; # un nouveau tableau de hachage, dans la cas où il n'y a pas de fichier Sereal enregistré

  opendir(my $dir,"$FindBin::Bin/data/ngrams") or die("...");
  while (my $file = readdir($dir)) {
    if ( $file eq $ngrams_sereal_name ){
      $trained_ngrams_hash = Sereal::read_sereal("$FindBin::Bin/data/ngrams/$ngrams_sereal_name");
    }
  }
  closedir($dir);

  return $trained_ngrams_hash;
}

sub generateMatrix {
  #   Génère la matrice de n-grams d'un texte en enrtée en ratio.
  # -----------------------------------------
  # pre :
  #   - $input_file  (STR) : en argument, le fichier pour lequel il faut générer la matrice
  # post :
  #   - retourne une REF(%{table de hachage}) contenant les données statistiques (ratio) de n-grams dans le texte d'entrée

  my $input_file = shift; #PRECONDITION 1

  my $ngrams_hash = file_to_ngram_occur( $input_file ); # génération d'un hash des occurences de ngram de car.
  $ngrams_hash = occ_2_ratio( $ngrams_hash ); # convertir les occurences en ratio dans le hash

  return $ngrams_hash;

  sub file_to_ngram_occur {
    #  Génère un hash des occurences de n-grams de caractères
    # -----------------------------------------
    # pre :
    #   - $input_file  (STR) : en argument, le fichier pour lequel il faut générer la matrice
    # post :
    #   - retourne une référence à une %table de hachage contenant le nombre d'occurence (valeur) par ngram (key)
    my $input_file = shift; #PRECONDITION 1

    # ouvrir le fichier d'input
    open( my $input_text, '<', $input_file )
  	 or die( "Impossible d'ouvrir $input_file" );
    binmode( $input_text, ':encoding(UTF-8)' );

    my $ngrams_hash = {}; # les occurences de ngrams seront listée ici
    while ( my $ligne = <$input_text> ) {
      chomp( $ligne );
      if ( ($ligne) and ( $ligne !~ /^\s*$/ ) ){
        # Si la ligne n'est pas vide
        $ligne =~ s/[\r\t\n\f]|\W|\p{Number}|\p{Punct}//g;
        # extraires tous les n-grams de caractères de la ligne. Ajouter 1 à chaque occurence du ngrams
        # séparer la ligne en mots sur les espaces
        my @mots = split(/ /, $ligne);
        foreach my $mot ( @mots ) {
          # on compte d'abord les unigrams (pour avoir la fréquence des caractères seuls)
          for ( my $str_index = 0 ; $str_index <= length( $mot ) ; $str_index++ ) {
            my $ngram = substr( $mot, $str_index, 1 );
            # Ajout de l'unigram, ou incrémentation si déjà présent
            if ( $ngrams_hash->{$ngram} ){
              $ngrams_hash->{$ngram}++;
            } else {
              $ngrams_hash->{$ngram} = 1;
            }
          }
          # compter de 2-gram jusqu'au 5-gram
          $mot = "_" . $mot . "_"; # on concatène un séparateur, qui indique le début et la fin du mot
          for ( my $n = 2 ; $n <= 5; $n++){
            for ( my $str_index = 0 ; $str_index <= length( $mot ) ; $str_index++ ) {
              my $ngram = substr( $mot, $str_index, $n );
              # Ajout de l'unigram, ou incrémentation si déjà présent
              if ( length( $ngram ) == $n ) {

                if ( $ngrams_hash->{$ngram} ){
                  $ngrams_hash->{$ngram} += 1;
                } else {
                  $ngrams_hash->{$ngram} = 1;
                }
              }
            }
            $mot = $mot . "_"; #on ajoute le symbole séprateur supplémentaire pour le ngram suivant
          }
        }
      }
    } # on a fini de lire tout le fichier
    close ( $input_text );
    return $ngrams_hash;
  }

  sub occ_2_ratio{
    #  Transforme les valeurs d'une table de hachache (en référence) depuis
    #  le nombre d'occurences vers un ratio. Ex : {"ba" => 1, ...} vers {"ba" => 0.1}
    # -----------------------------------------
    # pre :
    #   - $input_file  (REF{%}) : en argument, le fichier pour lequel il faut générer la matrice
    # post :
    #   - modifie les valeurs de la table de hashage grâce à sa référence (occurences -> ratio)
    #   - return la référence à la table de hashage
    my $hash = shift; #PRECONDITION 1
    my $nbr_of_ngrams  = %{$hash}; #on compte le nombre d'entrée du hash

    foreach my $key (keys %{$hash}){
      $hash->{$key} = ($hash->{$key} / $nbr_of_ngrams) * 1000; #on multiplie le ratio par 1000 pour la lisibilité
      # on calcul le ratio sur base du nombre total d'ngram du hash (occurence/nombre_total)
    }
    return $hash;
  }
}
sub printhash {
  # ----------------------
  # print le contenu d'un hache (si plusieur hash imbriqué par référence,
  # n'entre pas dans les sous niveaux).
  # ----------------------
  my $hash = shift;
  while (my ($k,$v)=each %{$hash}){
    print "\t$k \t$v\n";
  }
}
sub list {
  # pre : /
  # post : renvoit les langues possibles pour cet algorithme en REF(@TABLEAU)

  return [ sort ( keys ( %{load_stored_hash()} ) ) ] ;
}
sub print_list {
  # imprime les langues disponibles dans cet algorithme
  # pre : /
  # post print les langues disponibles
  my $list = list();
  print("== L'algorithme 'NGRAM' peut détecter les langues suivantes : \n");
  my $i = 1;
  foreach my $langue ( @{$list} ){
    print("\t$i.  $langue\n");
    $i++;
  }
}

1;
