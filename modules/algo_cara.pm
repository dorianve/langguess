#============================================================
# Package :	algo_cara.pm
# Auteur : 	Dorian Venderick <dorian.venderick@student.uclouvain.be>
# Date : 	09/06/2019
#============================================================
# Ce package tente de deviner la langue d'un texte donné en entrée en cherchant les caractères spéciaux spécifiques à une langue dans le texte d'entrée.
#============================================================
# Usage :
# doit être importée dans le fichier principal ../langguess.pl
#============================================================
package algo_cara;
use strict;
use warnings;
use utf8;
binmode(STDOUT,':encoding(UTF-8)');

my %langues = ( "anglais"   => 0,
                "francais"  => 0,
                "allemand"  => 0,
                "italien"   => 0,
                "russe"     => 0,
                "grec"   => 0 );

sub guessLang {
  # -----------------------------------------
  # Description :
  # Devine le langage du texte d'entrée en fonction des caractères spéciaux
  # -----------------------------------------
  # pre : $input_file en argument, le fichier pour lequel l'on cherche la langue
  # post : retourne la langue la plus probable
  my $input_file = shift;

  my $specCara = 0; # change en 1 si un caractère spéciale est trouvé.

  # ouvrir le fichier d'input
  open( my $input_text, '<', $input_file )
	 or die( "Impossible d'ouvrir $input_file" );
  binmode($input_text, ':encoding(UTF-8)');

  my $numLigne = 0; #compteur de lignes, pour ne pas avoir à parcourir tout le document avant de choisir de quelle langue il s'agit.

  while ( my $ligne = <$input_text> ) {
    if ($numLigne >= 400){
      last;
    }
  	chomp($ligne);
    ### Test de caractères latins
    if ($ligne =~ /[A-Za-z]/) {
      # reconaître l'alphabet latin dans la ligne
      $langues{"anglais"}   += 1;
        # S'il n'y a pas de caractères spéciaux d'autres langues, alors on ajoute un peu de poids à l'anglais
        if ( $ligne !~
          /[ĻǭửểỰÜủÈỴŲįėęŠắỈŬĆỔĖỶîŇỄĎŜĮţĄŞỨŷÉÔãỦćðõņŖỂńĞÕẹĈŴÙỊÇẵÒồþĒşÁẢâẠỖǪÂổĀňỡỜĴễëưųỀñÖŏẩẼợĐẸŽíỐŁūỗÊĳüởỉỚŐĽËýẾďǫúùÿấếỮŒğẳĵéôỸÑÐŢĶảŕƠứäŪēỞÄỌĂìĕĉƯ¿ĢŶỎł́ỘÛØꞗœĪÅỵŘŝżóỹỳĹŵẮÌụẶữỒľĝÝļạịöỠĘïīāỢỆĩĨọťßČẨáòŭōÓŤśÃẴỬỪķỷŮĜģÞçẺẤờ’ÆêŻẽừůÚĚÀŗřự᷄ớŔ̄ûẲøặĤűăŌẬẪåÍốĥæệũẦŨŸÏỲÎỏ¡đằěẻơẫŅźİỤśÃẴầèàềőẰŰậąĺŃ]/  ) {
            $langues{"anglais"}   += 0.1;
            # On en ne peut ni :
              # 1) seulement donner 1 poids à l'anglais quand il n'y a aucun caractère des autres langues latines, car le désavangaterait par rapport aux autre langues
              # 2) Donner un point complet quand il n'y a pas de caractère spéciaux sur une ligne : c'est courant dans d'autres langues aussi.
        }
      $langues{"francais"}  += 1;
        if ( $ligne =~ /[ÀÂÇÉÈÊËÎÏÔŒÙÛàâçéèêëîïôœùû]/ ) { $langues{"francais"}   += 1 }
      $langues{"allemand"}  += 1;
        if ( $ligne =~ /[ÄÖÜäöüß]/ ) { $langues{"allemand"}  += 1; }
      $langues{"italien"}   += 1;
        if ( $ligne =~ /[ÀÉÈÌÒÙàéèìòù]/ ) { $langues{"italien"}  += 1; }
    }
    if ( $ligne =~ /[АБВГДЕЖЗИКЛМНОПРСТУФХЦЧШЙЩЬЮЯЁЫЭ]/ ){ #cyrillique
      $langues{"russe"}  += 1;
    }
    if ( $ligne =~/[ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩαβγδεζηθικλμνξοπρσςτυφχψω]/){
      $langues{"grec"}  += 1;
    }
  $numLigne++;
  }
  close $input_text;
  return bestlanguagematch(\%langues);
}
sub bestlanguagematch {
  # pre : $langues  REF(Hash) : réfère à un hash contenant les probabilités des langues
  # post : imprime et retourne la langue avec le score le plus élevé
  my $langues = shift;
  my $max_value = 0;
  my $bestmatch = undef;
  while (my ($langue,$valeur)=each %{$langues}){
    if ($valeur >= $max_value) {
      $max_value = $valeur;
      $bestmatch = $langue;
    }
  }
  if ($bestmatch){
    print ("\t" . $bestmatch . "\n") ;
    return $bestmatch;
  } else {
    print("Aucune langue n'a pu être identifiée\n");
    return -1;
  }
}
sub list {
  # pre : /
  # post : renvoit les langues possibles pour cet algorithme en REF(@TABLEAU)
  return my $list = [ sort ( keys (%langues) ) ] ;
}
sub print_list {
  # imprime les langues disponibles dans cet algorithme
  # pre : /
  # post print les langues disponibles
  my $list = list();
  print("== L'algorithme 'CARA' peut détecter les langues suivantes : \n");
  my $i = 1;
  foreach my $langue ( @{$list} ){
    print("\t$i.  $langue\n");
    $i++;
  }
}
1;
