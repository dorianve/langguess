# Langguess : deviner la langue d'un texte (pas maintenu)

Le code présent dans ce Git est le résultat d'un travail dans le cadre du cours d'introduction à la programmation et au langage perl *LFIAL2630	Méthodologie du traitement informatique des données textuelles* à l'[UCLouvain](https://uclouvain.be/).

Ce programme permet de déterminer la langue originale d'un texte entre 4 langues (français, anglais, italien, allemand), bien que d'autres langues puissent être ajoutées par l'utilisateur.

Pour reconnaître automatiquement la langue, 3 approches sont disponibles : 

1. reconnaissance par caractères spéciaux (https://en.wikipedia.org/wiki/Wikipedia:Language_recognition_chart)
2. par dictionnaire (utilisant des dictionnaires d'[Unitex](https://unitexgramlab.org))
3. par n-grams de caractères (suivant l'[approche de Cavnar et Trenkle](http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.53.9367)). Il y a un problème d'implémentation à la dernière étape de génération de profile de fréquences de N-grams (j'utilise un ratio plutôt que l'ordre de fréquence de N-grams), ce qui diminue l'efficacité de l'algorithme.

Rappelons que langguess est un travail de cours (d'introduction à la programmation et à perl) et qu'il n'est pas maintenu. Plusieurs améliorations évidentes au niveau du code et des implémentations devraient être apportées pour que langguess soit plus intéressant.

Pour plus d'information sur ce travail (rendu en juin 2019), vous pouvez lire le rapport au format pdf, présent à la racin de ce répertoire.